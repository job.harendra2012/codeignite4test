# CodeIgniter 4 Application Starter

## Algorithms
 ## Factorial URL
  http://localhost:8080/factorial/4
 ## Tree Constructor URL
  http://localhost:8080/check-tree-constructor

## Command need to run
composer install
php spark migrate    
php spark db:seed CategorySeeder
php spark db:seed ProductSeeder
php spark db:seed CategoryProductSeeder
php spark db:seed UserSeeder
php spark db:seed UserAddressSeeder
php spark serve


## Category Tree
 http://localhost:8080/categories

## Cart
 http://localhost:8080/cart/add/1/1
 http://localhost:8080/cart/update/1/1
 http://localhost:8080/cart/remove/1
 http://localhost:8080/cart/delete

 ## Checkout URL
  http://localhost:8080/checkout
  http://localhost:8080/checkout/place-order
  http://localhost:8080/checkout/success/1

## Order List
  http://localhost:8080/orders
  http://localhost:8080/order/1
  http://localhost:8080/download/