<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

use App\Controllers\Algorithms;
use App\Controllers\Products;
use App\Controllers\Cart;
use App\Controllers\Checkout;
use App\Controllers\Category;
use App\Controllers\Order;

$routes->get('/', 'Home::index');

$routes->get('factorial/(:num)', [Algorithms::class, 'calculateFactorial']);
$routes->get('check-tree-constructor', [Algorithms::class, 'checkTreeConstructor']);

$routes->get('categories', [Category::class, 'index']);


$routes->get('products/?(:any)', [Products::class, 'index']);
$routes->get('product/(:num)', [Products::class, 'getProductDetails']);


$routes->get('cart', [Cart::class, 'index']);
// For testing , I am using get method otherwise it will change accordingly post, put ,delete
$routes->get('cart/add/(:num)/(:num)', [Cart::class, 'addToCart']);
$routes->get('cart/update/(:num)/(:num)', [Cart::class, 'updateItemQty']);
$routes->get('cart/remove/(:num)', [Cart::class, 'removeItem']);
$routes->get('cart/delete', [Cart::class, 'destroy']);


$routes->get('checkout', [Checkout::class, 'index']);
$routes->get('checkout/place-order', [Checkout::class, 'placeOrder']);
$routes->get('checkout/success/(:num)', [Checkout::class, 'orderSuccess']);


$routes->get('orders', [Order::class, 'index']);
$routes->get('order/(:any)', [Order::class, 'getOrderItems']);
$routes->get('order/download/(:any)', [Order::class, 'downloadDigitalProduct']);







/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
