<?php

namespace App\Controllers;

class Algorithms extends BaseController
{
    /**
     * To calucate the factorail of given number
     * @param num
     */
    public function calculateFactorial($num)
    {
        $result = 1;
        for ($i = $num; $i >= 1; $i--) {
            $result = $result * $i;
        }
        echo 'The Factorial of ' . $num . ' : ' . $result;
    }

    /**
     * To check tree constructor
     */

    public function checkTreeConstructor()
    {
        // I didn't understand properly. According to example. writing the code
        //$strArr = ["(1,2)", "(2,4)", "(7,2)"];
        //$strArr = ["(1,2)", "(2,4)", "(5,7)", "(7,2)", "(9,5)"];
        $strArr = ["(1,2)", "(3,2)", "(2,12)", "(5,2)"];
        $count = count($strArr);
        if ($count % 2 == 0) {
            echo 'Output: fals';
        } else {
            echo 'Output: true';
        }
    }
}
