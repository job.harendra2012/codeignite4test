<?php

namespace App\Controllers;

use App\Models\ProductModel;

class Cart extends BaseController
{


    /**
     * Display the list od cart product
     * @return view
     */
    function index()
    {
        try {
            $carts  =  session('cart');
            $data = [
                'carts'  => $carts,
                'title' => 'Carts',
            ];
            return view('shared/header', $data)
                . view('cart/index')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;
            print_r($th->getMessage());
        }
    } //end index

    /**
     * Add Product into a card
     * @param id  
     * @param quantity
     * @update cart session
     */
    // URL:  http://localhost:8080/cart/add/1/1
    function addToCart($id, $quantity)
    {
        try {
            $productModel = new ProductModel();
            // Fetch the product details by  id
            //Here also we can check product status and stock
            $product = $productModel->find($id);
            if (!empty($product)) {
                $session = session();
                if ($session->has('cart')) {
                    $cart = session('cart');
                } else {
                    $cart = [];
                }
                $price = ($product['product_selling_price'] * $quantity);
                if (array_key_exists($id, $cart)) {
                    $quantity = $quantity + $cart[$id]['quantity'];
                    $price = ($product['product_selling_price'] * $quantity);
                }
                $cart[$id] =  [
                    'id' => $product['id'],
                    'article_number' => $product['article_number'],
                    'name' => $product['name'],
                    // 'product_mrp' => $product['product_mrp'],
                    // 'product_discount' => $product['product_discount'],
                    'product_selling_price' => $product['product_selling_price'],
                    'price' => $price,
                    'quantity' => $quantity,
                ];
                $session->set('cart', $cart);
            }
            return $this->response->redirect(site_url('cart'));
        } catch (\Throwable $th) {
            //throw $th;
            return $this->response->redirect(site_url('cart'));
        }
    } //end addToCart

    /**
     * Update quantity of product
     * @param id  
     * @param quantity
     * @update cart session
     */
    // URL:  http://localhost:8080/cart/update/1/1
    function updateItemQty($id, $quantity)
    {
        try {
            $session = session();
            if ($session->has('cart')) {
                $cart = session('cart');
                if (array_key_exists($id, $cart)) {
                    $cart[$id]['quantity'] = $quantity;
                    $cart[$id]['price'] = $quantity * $cart[$id]['product_selling_price'];
                    $session->set('cart', $cart);
                    return $this->response->redirect(site_url('cart'));
                } else {
                    $this->addToCart($id, $quantity);
                }
            } else {
                $this->addToCart($id, $quantity);
            }
        } catch (\Throwable $th) {
            return $this->response->redirect(site_url('cart'));
        }
    } //end updateItemQty


    /**
     * Remove product from cart
     * @param  id  number
     * @update cart session
     */
    // URL:  http://localhost:8080/cart/remove/1
    function removeItem($id)
    {
        try {
            $session = session();
            if ($session->has('cart')) {
                $cart = session('cart');
                if (array_key_exists($id, $cart)) {
                    unset($cart[$id]);
                    $session->set('cart', $cart);
                }
            }
            return $this->response->redirect(site_url('cart'));
        } catch (\Throwable $th) {
            return $this->response->redirect(site_url('cart'));
        }
    }

    /**
     * Remove product from cart
     * @param  id  number
     * @update cart session
     */
    // URL:  http://localhost:8080/cart/delete
    public function destroy()
    {
        try {
            $session = session();
            if ($session->has('cart')) {
                $session->remove('cart');
            }
            return $this->response->redirect(site_url('cart'));
        } catch (\Throwable $th) {
            return $this->response->redirect(site_url('cart'));
        }
    } //end destroy
}
