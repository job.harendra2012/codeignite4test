<?php

namespace App\Controllers;

use App\Models\CategoryModel;

class Category extends BaseController
{
    /**
     * Display the tree categories
     * @return view
     */
    function index($slug = null)
    {
        try {
            $categoryModel =  new CategoryModel();
            $data = [
                'categories'  => $categoryModel->getCategoryTree(0),
                'title' => 'Category List',
            ];
            return view('shared/header', $data)
                . view('category/index')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;
        }
    } //end index


}
