<?php

namespace App\Controllers;

use App\Models\ProductModel;
use App\Models\OrderModel;
use App\Models\OrderItemModel;
use App\Models\UserAddressModel;

class Checkout extends BaseController
{
    /**
     * Display the list of cart items
     * @return view
     */
    function index()
    {
        try {
            $session  =  session();
            if ($session->has('cart')) {
                $cart  =  session('cart');
                if (count($cart) == 0) {
                    return $this->response->redirect(site_url('cart'));
                }
            } else {
                return $this->response->redirect(site_url('cart'));
            }

            $data = [
                'carts'  => $cart,
                'title' => 'Checkout Page'
            ];
            return view('shared/header', $data)
                . view('checkout/index')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;
            return $this->response->redirect(site_url('cart'));
        }
    } //end index

    /**
     * Save the list of cart items
     * @return view
     */

    public function placeOrder()
    {
        try {
            //Assuming user id 1 placing order otherwise I have to  login session  user or allow guest user 
            $userId = 1;
            $userAddressId = 1;
            $session  =  session();

            if ($session->has('cart')) {
                $cart  =  session('cart');
                if (count($cart) == 0) {
                    // Through the error otherwise redirect to cart page
                    return $this->response->redirect(site_url('cart'));
                } else {
                    $cartItems = [];
                    // $toal_product_mrp = 0;
                    // $total_product_discount = 0;
                    $total_product_selling_price = 0;
                    // Asuming there is no promotion code / coupon code applied
                    // validation the stock and price changes
                    // We can get all products of cart in sngle query then we can check but for Now, I am checking single product  at a time

                    foreach ($cart as $productId => $c) {
                        $productModel = new ProductModel();
                        $product = $productModel->find($productId);
                        if (!empty($product)) {
                            if ($product['product_selling_price'] == $c['product_selling_price']) {

                                if (($product['product_type'] == 'normal' && $product['stock'] < $c['quantity'])) {
                                    // Through the error price has been change or some proudct is outof stock otherwise redirect to cart page
                                    return $this->response->redirect(site_url('cart'));
                                }

                                // $toal_product_mrp = $toal_product_mrp + ($c['quantity'] * $product['product_mrp']);
                                // $total_product_discount = $total_product_discount + ($c['quantity'] * $product['product_discount']);
                                $total = ($c['quantity'] * $product['product_selling_price']);
                                $total_product_selling_price = $total_product_selling_price + $total;

                                $items = [
                                    'order_id' => null,
                                    'product_id' => $product['id'],
                                    'product_mrp' => $product['product_mrp'],
                                    'product_discount' => $product['product_discount'],
                                    'product_selling_price' => $product['product_selling_price'],
                                    'quantity' => $c['quantity'],
                                    'total' => $total
                                ];
                                array_push($cartItems, $items);
                            } else {
                                // Through the error price has been change or some proudct is outof stock otherwise redirect to cart page
                                return $this->response->redirect(site_url('cart'));
                            }
                        } else {
                            // Through the error otherwise redirect to cart page
                            return $this->response->redirect(site_url('cart'));
                        }
                    }

                    if (count($cartItems) > 0) {
                        $userAddressModel = new UserAddressModel();
                        $addressRow = $userAddressModel->find($userAddressId);
                        $address = [
                            'name' => $addressRow['name'],
                            'contat_number' => $addressRow['contat_number'],
                            'country' => $addressRow['country'],
                            'state' => $addressRow['state'],
                            'city' => $addressRow['city'],
                            'pincode' => $addressRow['pincode'],
                            'address' => $addressRow['address'],
                        ];

                        // Saving cart items in table
                        $orderModel = new OrderModel();
                        $data = [
                            'user_id' => $userId,
                            'items_amount' => $total_product_selling_price,
                            'delivery_charge' => 0,
                            'total' => $total_product_selling_price,
                            'promotion_code' => null,
                            'promotion_amount' => 0,
                            'grand_total' => $total_product_selling_price,
                            'delivery_address' => json_encode($address),
                            'order_status' => 'success'
                        ];
                        $orderModel->insert($data);
                        $orderId = $orderModel->getInsertID();

                        foreach ($cartItems as $index => $cartItem) {
                            $cartItems[$index]['order_id'] = $orderId;
                        }
                        $orderModelItems = new OrderItemModel();
                        $orderModelItems->insertBatch($cartItems);
                        $session->remove('cart');
                        // Redirect after saving record successfully. We can use temporarily session or parameter to pass order id in success page
                        return $this->response->redirect(site_url('checkout/success/' . $orderId));
                    }
                }
            } else {
                // Through the error otherwise redirect to cart page
                return $this->response->redirect(site_url('cart'));
            }
        } catch (\Throwable $th) {
            // Through the error otherwise redirect to cart page
            //return $this->response->redirect(site_url('cart'));
        }
    } //end placeOrder



    /**
     * Display the list of cart items
     * @return view
     */
    function orderSuccess($orderId)
    {
        try {
            //Checking order is exist or not
            $orderModel = new OrderModel();
            $order = $orderModel->find($orderId);
            if (empty($order)) {
                // Through the error otherwise redirect to cart page
                return $this->response->redirect(site_url('cart'));
            }
            $data = [
                'ordre' => $order,
                'title' => 'Order Success page'
            ];
            return view('shared/header', $data)
                . view('checkout/order-success')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;
            return $this->response->redirect(site_url('cart'));
        }
    } //end orderSuccess
}
