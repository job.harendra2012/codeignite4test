<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\OrderModel;
use App\Models\OrderItemModel;

class Order extends BaseController
{
    /**
     * Display the list od products
     * @return view
     */
    function index()
    {
        try {
            $data = [
                'orders'  => [],
                'title' => 'Order List',
            ];
            return view('shared/header', $data)
                . view('order/index')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;

        }
    } //end index


    /**
     * Display the product list of particular order
     * @param order_id 
     * @return view
     */
    function getOrderItems($order_id)
    {
        try {
            $data = [
                'order'  => [],
                'orderItems' => [],
                'title' => 'Order Item List',
            ];
            return view('shared/header', $data)
                . view('order/index')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;
        }
    } //end getOrderItems



    /**
     * Download link for digital product
     * @param product_id
     */
    function downloadDigitalProduct($product_id)
    {
    } //end downloadDigitalProduct


    /**
     * Download link for digital product
     * @param order_id
     */
    function sendReuestDeliveryAPI($order_id)
    {
    } //end sendReuestDeliveryAPI



}
