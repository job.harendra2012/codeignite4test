<?php

namespace App\Controllers;

use App\Models\ProductModel;

class Products extends BaseController
{

    /**
     * Display the list od products
     * @param  slug string( Category slug)
     * @return view
     */
    function index($slug = null)
    {
        try {
            $productModel =  new ProductModel();
            $data = [
                'products'  => $productModel->getAllProduct($slug),
                'title' => 'Product List',
                'slug' => $slug
            ];
            return view('shared/header', $data)
                . view('products/index')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;
        }
    } //end index

    /**
     * Display the details of a product
     * @param  id string( product id)
     * @return view
     */
    function getProductDetails($id)
    {
        try {
            $productModel = new ProductModel();
            $data = [
                'product'  => $productModel->find($id),
                'title' => 'Product Details',
                'id' => $id
            ];
            return view('shared/header', $data)
                . view('products/details')
                . view('shared/footer');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


    /**
     * Insert the product
     */
    function insertProduct()
    {
        // Product of name
        $name = '';

        // Article number of product
        $article_number = '';


        /*
         *Product type
         * normal: Normal products
         * digital: Digital products
         */
        $product_type = 'normal';

        // Product MRP price
        $product_mrp = '';

        /*
         **Discount on MRP price
         *  percentage: Discount in precentage and it should be less than or equal to 100
         *  fixed: Fixed Amount, it should b  less than or equal to product mrp price
         */
        $product_discount_type = 'percentage';

        //Discount product value accrording to  discount type
        $product_discount = '';

        // Calculated price accound to MRP and discount
        $product_selling_price = '';

        // Stoke manage according to product type if product is normal then stock should be required
        $stock = '';

        // For upto 5 images URL
        $image_1 = '';
        $image_2 = '';
        $image_3 = '';
        $image_4 = '';
        $image_5 = '';

        /*
         *Product Status
         *  active: Product display on front end(Customer can buy it)
         *  inactive: Product cannot display on front end(Customer cannot buy it) 
         *  out-of-stock: Product display on front end(Customer cannot buy it), It is only for normal product
         */
        $status = 'active';

        // Proudct attached with the selecte categories
        $this->attachCateogry([1, 2, 3]);
    }

    /**
     * Attach the product with particular categories
     */
    private function attachCateogry($catory_ids = [])
    {
    }

    /**
     * Deattach the product with all attached categoryF
     */
    private function deAttachCateogry($prudct_id)
    {
    }

    /**
     * Update the product
     */
    function updateProduct()
    {
        // Product of name
        $name = '';

        // Article number of product
        $article_number = '';


        /*
                 *Product type
                 * normal: Normal products
                 * digital: Digital products
                 */
        $product_type = 'normal';

        // Product MRP price
        $product_mrp = '';

        /*
                 **Discount on MRP price
                 *  percentage: Discount in precentage and it should be less than or equal to 100
                 *  fixed: Fixed Amount, it should b  less than or equal to product mrp price
                 */
        $product_discount_type = 'percentage';

        //Discount product value accrording to  discount type
        $product_discount = '';

        // Calculated price accound to MRP and discount
        $product_selling_price = '';

        // Stoke manage according to product type if product is normal then stock should be required
        $stock = '';

        // For upto 5 images URL
        $image_1 = '';
        $image_2 = '';
        $image_3 = '';
        $image_4 = '';
        $image_5 = '';

        /*
                 *Product Status
                 *  active: Product display on front end(Customer can buy it)
                 *  inactive: Product cannot display on front end(Customer cannot buy it) 
                 *  out-of-stock: Product display on front end(Customer cannot buy it), It is only for normal product
                 */
        $status = 'active';


        // Deattached the product from existing category
        $this->deAttachCateogry(1);
        // Proudct attached with the selecte categories
        $this->attachCateogry([1, 2, 3]);


        //Any major update like price, stattus, stock
        $this->userNotification();
    }

    /**
     * Send notification email if any user subscribe or buy it if any major update in subcribed products
     */
    function userNotification()
    {
    }
}
