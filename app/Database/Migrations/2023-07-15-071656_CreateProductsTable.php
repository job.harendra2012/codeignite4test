<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use CodeIgniter\Database\RawSql;

class CreateProductsTable extends Migration
{
    public function up()
    {
        $fields = [
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'article_number' => [
                'type'       => 'INT',
                'constraint' => 5,
                'unsigned'       => true,
                'unique'     => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'default'    => null,
            ],
            'product_type' => [
                'type'       => 'ENUM',
                'constraint' => ['normal', 'digital'],
                'default'    => 'normal',
            ],
            'product_mrp' => [
                'type'       => 'DECIMAL',
                'constraint' => '10,2',
                'unsigned'   => true,
                'default'    => 0.00,
            ],
            'product_discount_type' => [
                'type'       => 'ENUM',
                'constraint' => ['percentage', 'fixed'],
                'default'    => 'percentage',
            ],
            'product_discount' => [
                'type'       => 'DECIMAL',
                'constraint' => '10, 2',
                'unsigned'   => true,
                'default'    => 0.00,
            ],
            // It shoulb be auto calulated value 
            'product_selling_price' => [
                'type'       => 'DECIMAL',
                'constraint' => '10, 2',
                'unsigned'       => true,
                'default'    => 0.00,
            ],
            'stock' => [
                'type'       => 'INT',
                'constraint' => 5,
                'unsigned'       => true,
                'default'     => null,
            ],
            // Product Images( It can be also managed in sepate table but  we have fix. It will go upto 5)

            'image_1' => [
                'type'       => 'VARCHAR',
                'constraint' => '500',
                'default'    => null,
            ],
            'image_2' => [
                'type'       => 'VARCHAR',
                'constraint' => '500',
                'default'    => null,
            ],
            'image_3' => [
                'type'       => 'VARCHAR',
                'constraint' => '500',
                'default'    => null,
            ],
            'image_4' => [
                'type'       => 'VARCHAR',
                'constraint' => '500',
                'default'    => null,
            ],
            'image_5' => [
                'type'       => 'VARCHAR',
                'constraint' => '500',
                'default'    => null,
            ],
            'status' => [
                'type'       => 'ENUM',
                'constraint' => ['active', 'inactive', 'out-of-stock'],
                'default'    => 'active',
            ],
            'created_at' => [
                'type'    => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type'    => 'TIMESTAMP',
                'default' => NULL,
            ],
        ];
        $this->forge->addField($fields);
        $this->forge->addKey('id', true);
        $this->forge->createTable('products');
    }

    public function down()
    {
        $this->forge->dropTable('products');
    }
}
