<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use CodeIgniter\Database\RawSql;

class CrerateUserAddressTable extends Migration
{
    public function up()
    {
        $fields = [
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'user_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'default'    => null,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'default'    => null,
            ],
            'contat_number' => [
                'type'       => 'VARCHAR',
                'constraint' => '15',
                'default'    => null,
            ],

            'country' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'default'    => null,
            ],
            'state' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'default'    => null,
            ],
            'city' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'default'    => null,
            ],
            'pincode' => [
                'type'       => 'VARCHAR',
                'constraint' => '10',
                'default'    => null,
            ],
            'address' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'default'    => null,
            ],
            'is_default_address' => [
                'type'       => 'ENUM',
                'constraint' => ['yes', 'no'],
                'default'    => 'no',
            ],
            'created_at' => [
                'type'    => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type'    => 'TIMESTAMP',
                'default' => NULL,
            ],
        ];
        $this->forge->addField($fields);
        $this->forge->addKey('id', true);
        $this->forge->createTable('useraddresses');
    }

    public function down()
    {
        $this->forge->dropTable('useraddresses');
    }
}
