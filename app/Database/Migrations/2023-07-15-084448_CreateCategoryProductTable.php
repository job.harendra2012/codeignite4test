<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateCategoryProductTable extends Migration
{
    public function up()
    {

        $fields = [
            'category_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
            'product_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
        ];
        $this->forge->addField($fields);
        $this->forge->createTable('category_product');
    }

    public function down()
    {
        $this->forge->dropTable('category_product');
    }
}
