<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateOrderTable extends Migration
{
    public function up()
    {
        $this->forge->addField(
            [
                'id' => [
                    'type' => 'INT',
                    'constraint' => 5,
                    'auto_increment' => TRUE,
                ],
                'user_id' => [
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                ],
                'items_amount' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'delivery_charge' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'total' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'promotion_code' => [
                    'type' => 'VARCHAR',
                    'constraint' => 50,
                    'default'    => null,
                ],
                'promotion_amount' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                //final amount
                'grand_total' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'delivery_address' => [
                    'type' => 'JSON',
                    'default'    => null,
                ],
                'order_status' => [
                    'type'       => 'ENUM',
                    'constraint' => ['success', 'failed'],
                    'default'    => null,
                ],
                'created_at datetime default current_timestamp',
            ]
        );
        $this->forge->addKey('id', true);
        $this->forge->createTable('orders');
    }

    public function down()
    {
        $this->forge->dropTable('orders');
    }
}
