<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateOrderItemTable extends Migration
{
    public function up()
    {
        $this->forge->addField(
            [
                'id' => [
                    'type' => 'INT',
                    'constraint' => 5,
                    'auto_increment' => TRUE,
                ],
                'order_id' => [
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                ],
                'product_id' => [
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                ],
                'product_mrp' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'product_discount' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'product_selling_price' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'quantity' => [
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                ],
                'total' => [
                    'type'       => 'DECIMAL',
                    'constraint' => '10,2',
                    'unsigned'   => true,
                    'default'    => 0.00,
                ],
                'created_at datetime default current_timestamp',
            ]
        );
        $this->forge->addKey('id', true);
        $this->forge->createTable('order_items');
    }

    public function down()
    {
        $this->forge->dropTable('order_items');
    }
}
