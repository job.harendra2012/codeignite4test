<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class CategoryProductSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'category_id' => 1,
                'product_id' => 1,
            ],
            [
                'category_id' => 1,
                'product_id' => 2,
            ],
            //Dell Vostro 3425 Laptop
            [
                'category_id' => 4,
                'product_id' => 3,
            ],
            [
                'category_id' => 3,
                'product_id' => 3,
            ],
            //HP Gaming Laptop 12th Gen
            [
                'category_id' => 4,
                'product_id' => 4,
            ],
            [
                'category_id' => 3,
                'product_id' => 4,
            ],
            [
                'category_id' => 9,
                'product_id' => 4,
            ],
        ];
        foreach($data as $row){
            $this->db->table('category_product')->insert($row);
        }
    }
}
