<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $data = [
            // 1
            [
                'name' => 'eBooks',
                'slug' => 'ebooks',
                'parent_id' => 0
            ],
            // 2
            [
                'name' => 'Photography',
                'slug' => 'photography ',
                'parent_id' => 0
            ],
            // 3
            [
                'name' => 'Laptops & Accessories',
                'slug' => 'laptops-accessories',
                'parent_id' => 0
            ],
            // 4
            [
                'name' => 'Laptops',
                'slug' => 'laptops',
                'parent_id' => 3
            ],
            //5
            [
                'name' => 'Tablets',
                'slug' => 'tablets',
                'parent_id' => 3
            ],
            //6
            [
                'name' => 'Data storage',
                'slug' => 'data-storage',
                'parent_id' => 3
            ],
            //7
            [
                'name' => 'Thin and light laptops',
                'slug' => 'thin-and-light-laptops',
                'parent_id' => 4
            ],
            //8
            [
                'name' => '2-in-1 Laptops',
                'slug' => '2-in-1-Laptops',
                'parent_id' => 4
            ],
            //9
            [
                'name' => 'Gaming laptops',
                'slug' => 'Gaming-laptops',
                'parent_id' => 4
            ],
        ];
        foreach($data as $row){
            $this->db->table('categories')->insert($row);
        }
    }
}
