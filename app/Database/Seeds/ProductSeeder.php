<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run()
    {

        $data = [
            // 1
            [
                'article_number' => 1001,
                'name' => 'English For General Competitions Vol',
                'product_type' => 'digital',
                'product_mrp' => 525.00,
                'product_discount_type' => 'fixed',
                'product_discount' => 145.00,
                'product_selling_price' => 380.00,
                'stock' => NULL,
                'image_1' => 'https://rukminim2.flixcart.com/image/416/416/l547yq80/regionalbooks/u/s/e/english-for-general-competitions-vol-1-english-medium-neetu-original-imagfvcf6rzgvzsf.jpeg?q=70',
                'image_2' => '',
                'image_3' => '',
                'image_4' => '',
                'image_5' => '',
                'status' => 'ative'

            ],
            // 2
            [
                'article_number' => 1002,
                'name' => 'Principles Of Indian Geography',
                'product_type' => 'digital',
                'product_mrp' => 699.00,
                'product_discount_type' => 'percentage',
                'product_discount' => 51.00,
                'product_selling_price' => 341.00,
                'stock' => NULL,
                'image_1' => 'https://rukminim2.flixcart.com/image/416/416/l3khsi80/regionalbooks/3/k/a/principles-of-indian-geography-english-1st-edition-for-upsc-cse-original-imagenkszyhpcmvt.jpeg?q=70',
                'image_2' => '',
                'image_3' => '',
                'image_4' => '',
                'image_5' => '',
                'status' => 'ative'

            ],
            //3
            [
                'article_number' => 1004,
                'name' => 'Dell Vostro 3425 Laptop',
                'product_type' => 'normal',
                'product_mrp' => 50837.00,
                'product_discount_type' => 'percentage',
                'product_discount' => 28.00,
                'product_selling_price' => 36490.00,
                'stock' => 100,
                'image_1' => 'https://m.media-amazon.com/images/I/51Esr+kss5L._SX679_.jpg',
                'image_2' => '',
                'image_3' => '',
                'image_4' => '',
                'image_5' => '',
                'status' => 'ative'

            ],
            //4
            [
                'article_number' => 1005,
                'name' => 'HP Gaming Laptop 12th Gen',
                'product_type' => 'normal',
                'product_mrp' => 108002.00,
                'product_discount_type' => 'percentage',
                'product_discount' => 35.00,
                'product_selling_price' => 69990.00,
                'stock' => 100,
                'image_1' => 'https://m.media-amazon.com/images/I/51rrWvzviVL._SX679_.jpg',
                'image_2' => '',
                'image_3' => '',
                'image_4' => '',
                'image_5' => '',
                'status' => 'ative'
            ],
        ];
        foreach($data as $row){
            $this->db->table('products')->insert($row);
        }
    }
}
