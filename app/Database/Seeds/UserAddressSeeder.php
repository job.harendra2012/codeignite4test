<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class UserAddressSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 2; $i++) {
            $this->db->table('useraddresses')->insert($this->generateUsers($i));
        }
    }

    private function generateUsers($i): array
    {
        $faker = Factory::create();
        return [
            'user_id' => $faker->numberBetween(1, 2),
            'name' => $faker->name(),
            'contat_number' => $faker->phoneNumber(),
            'country' => "Indai",
            'state' => 'Delhi',
            'city' => 'New Delhi',
            'pincode' => 110030,
            'address' => ($i == 0) ? 'Ghitoni' : 'Shaket',
            'is_default_address' => 'yes'

        ];
    }
}
