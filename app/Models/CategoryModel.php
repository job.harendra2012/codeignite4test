<?php

namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'categories';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];


    public function getCategoryTree($parent_id, $collection = [], $level = 0)
    {
        $categories =  $this->where('parent_id', $parent_id)->findAll();
        if (count($categories) > 0) {
            foreach ($categories as $c) {
                $id = $c['id'];
                $collection[$id] = [
                    'id' => $c['id'],
                    'name' => $c['name'],
                    'parent_id' => $c['parent_id'],
                    'level' => $level
                ];
                $collection = $this->getCategoryTree($c['id'], $collection, $level + 1);
            }
        }
        return $collection;
    }
}