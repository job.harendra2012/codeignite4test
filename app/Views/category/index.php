<main>
    <h2>Category Tree Table</h2>
    <?php if (isset($categories) &&  count($categories) > 0) { ?>
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Parent Id</th>
            </tr>
            <?php foreach ($categories as $c) { ?>
                <tr>
                    <td><?php echo $c['id']; ?></td>
                    <td><?php
                        for ($m = 0; $m <$c['level']; $m++) {
                            echo "&rarr;";
                        }

                        echo $c['name']; ?></td>
                    <td><?php echo $c['parent_id']; ?></td>

                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
    <?php } ?>
</main>


<style>
    .table {
        width: 80%;
        border-collapse: collapse;
        margin: 0 20px 0 20px;
    }

    .table td,
    .table th {
        border: 1px solid #ddd;
        padding: 10px;
        text-align: left;
    }

    .table td:last-child,
    .table th:last-child {
        text-align: center;
    }

    .table tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table tr:hover {
        background-color: #ddd;
    }

    .table th {
        padding-top: 12px;
        padding-bottom: 12px;

        background-color: #04AA6D;
        color: #fff;
    }
</style>