<main>
    <?php if (isset($carts) &&  count($carts) > 0) { ?>
        <table class="table">
            <tr>
                <th>Article Number</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
            </tr>
            <?php foreach ($carts as $c) { ?>
                <tr>
                    <td><?php echo $c['article_number']; ?></td>
                    <td><?php echo $c['name']; ?></td>
                    <td><?php echo $c['quantity']; ?></td>
                    <td><?php echo $c['price']; ?></td>
                </tr>

            <?php } ?>
            <tr>
                <td colspan="4" style="text-align: end;">
                    <b>Grand Total</b>: <?php echo array_sum(array_column($carts, 'price')) ?>
                </td>
            </tr>
        </table>
    <?php } else { ?>
        <div>Your cart is empty</div>
    <?php } ?>
</main>


<style>
    .table {
        width: 80%;
        border-collapse: collapse;
        margin: 0 20px 0 20px;
    }

    .table td,
    .table th {
        border: 1px solid #ddd;
        padding: 10px;
        text-align: left;
    }

    .table td:last-child,
    .table th:last-child {
        text-align: center;
    }

    .table tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table tr:hover {
        background-color: #ddd;
    }

    .table th {
        padding-top: 12px;
        padding-bottom: 12px;

        background-color: #04AA6D;
        color: #fff;
    }
</style>