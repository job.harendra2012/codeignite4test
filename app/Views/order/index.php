<main>
    <?php if (isset($orders) &&  count($orders) > 0) { ?>
        <table class="table">
            <tr>
                <th>Order Id</th>
                <th>User Name</th>
                <th>Items Amount</th>
                <th>Delivery Charge</th>
                <th>Total</th>
                <th>Promotion Code</th>
                <th>Promotion Amount</th>
                <th>Grand Total</th>
                <th>Status</th>
            </tr>
            <?php foreach ($orders as $item) { ?>
                <tr>
                    <td><?php echo $item['id']; ?></td>
                    <td><?php echo $item['user_id']; ?></td>
                    <td><?php echo $item['items_amount']; ?></td>
                    <td><?php echo $item['delivery_charge']; ?></td>
                    <td><?php echo $item['total']; ?></td>
                    <td><?php echo $item['promotion_code']; ?></td>
                    <td><?php echo $item['promotion_amount']; ?></td>
                    <td><?php echo $item['grand_total']; ?></td>
                    <td><?php echo $item['order_status']; ?></td>
                </tr>

            <?php } ?>
        </table>
    <?php } else { ?>
        <div>No record found</div>
    <?php } ?>
</main>


<style>
    .table {
        width: 80%;
        border-collapse: collapse;
        margin: 0 20px 0 20px;
    }

    .table td,
    .table th {
        border: 1px solid #ddd;
        padding: 10px;
        text-align: left;
    }

    .table td:last-child,
    .table th:last-child {
        text-align: center;
    }

    .table tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table tr:hover {
        background-color: #ddd;
    }

    .table th {
        padding-top: 12px;
        padding-bottom: 12px;

        background-color: #04AA6D;
        color: #fff;
    }
</style>